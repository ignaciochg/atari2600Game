; move a happy face with PlayerBufferStuffer
   processor 6502
   include vcs.h
   org $F000
        
score = #0        
;plater
YPosFromBot = $80;
VisiblePlayerLine = $81;
PlayerBuffer = $82 ;setup an extra variable
;target
YPosFromBotTarget = $80;
VisibleTargetLine = $81;
TargetBuffer = $82 ;setup an extra variable

;misile
YPosFromBotMissile = $80;
VisibleMissileLine = $81;
MissileEnabled = #0

;generic start up stuff...
Start
   SEI   
   CLD   
   LDX #$FF 
   TXS   
   LDA #0      
ClearMem 
   STA 0,X     
   DEX      
   BNE ClearMem   
   
   LDA #$00   ;start with a black background
   STA COLUBK  
   LDA #$1C   ;set color for player 0
   STA COLUP0

         LDA #$1C   ;set color for player 1 AKA target
   STA COLUP1
        
;Setting some variables...
   LDA #80 ;player initial position
   STA YPosFromBot   ;Initial Y Position
   STA YPosFromBotMissile  ;Initial Y Position

;; setup misile 0
   LDA #2
   STA ENAM0  ;enable it
   LDA #33
   STA COLUP0 ;color it

   LDA #$60 
   STA NUSIZ0  ;setting misile with
   STA NUSIZ1  ;setting misile with

   
   LDA #$c9 ; -1 in the left nibble
   STA HMM0 ; of HMM1 sets it to moving
   LDA #2 ;enable misile to player 0 lock
        STA RESMP0

;VSYNC time
MainLoop
   LDA #2
   STA VSYNC   
   STA WSYNC   
   STA WSYNC   
   STA WSYNC   
   LDA #43  
   STA TIM64T  
   LDA #0
   STA VSYNC   


; for up and down, we INC or DEC
; the Y Position

   jsr CheckControls
   jsr CheckCollision
   

   LDA #0       ;zero out the buffer
   STA PlayerBuffer ;just in case
   STA TargetBuffer ;just in case

WaitForVblankEnd
   LDA INTIM   
   BNE WaitForVblankEnd 
   LDY #191    


   STA WSYNC   
   STA HMOVE   
   
   STA VBLANK     


;main scanline loop...


ScanLoop 
   STA WSYNC   

   LDA PlayerBuffer ;buffer was set during last scanline
   STA GRP0         ;put it as graphics now
   LDA TargetBuffer ;buffer was set during last scanline
   STA GRP1         ;put it as graphics now

;CheckActivatePlayer
;  CPY YPosFromBot
;  BNE SkipActivatePlayer
;  LDA #8
;  STA VisiblePlayerLine 
;SkipActivatePlayer
CheckActivatePlayer1
   CPY YPosFromBotTarget
   BNE SkipActivatePlayer1
   LDA #8
   STA VisibleTargetLine 
SkipActivatePlayer1  

;CheckActivateMissile
;  CPY YPosFromBotMissile  ;compare Y to the YPosFromBot...
;  BNE SkipActivateMissile ;if not equal, skip this...
;  LDA #8         ;otherwise say that this should go
;  STA VisibleMissileLine  ;on for 8 lines   
;SkipActivateMissile


;set player bufferto all zeros for this line, and then see if 
;we need to load it with graphic data
   LDA #0      
   STA PlayerBuffer   ;set buffer, not GRP0
   STA TargetBuffer   ;set buffer, not GRP0
   ;STA ENAM0
;
;
;if the VisiblePlayerLine is non zero,
;we're drawing it next line
;

;  LDX VisiblePlayerLine   ;check the visible player line...
;  BEQ FinishPlayer  ;skip the drawing if its zero...
IsPlayerOn

;  LDA BigHeadGraphic-1,X  ;otherwise, load the correct line from BigHeadGraphic
            ;section below... it's off by 1 though, since at zero
            ;we stop drawing
;  STA PlayerBuffer  ;put that line as player graphic for the next line
;  DEC VisiblePlayerLine   ;and decrement the line count
;FinishPlayer


   LDX VisibleTargetLine   ;check the visible player line...
   BEQ FinishPlayer1 ;skip the drawing if its zero...
IsPlayerOn1
   LDA TargetGraphic-1,X   ;otherwise, load the correct line from BigHeadGraphic
            ;section below... it's off by 1 though, since at zero
            ;we stop drawing
   STA TargetBuffer  ;put that line as player graphic for the next line
   DEC VisibleTargetLine   ;and decrement the line count
FinishPlayer1





;if the VisibleMissileLine is non zero,
;we're drawing it
;
;  LDA VisibleMissileLine  ;load the value of what missile line we're showing
;  BEQ FinishMissile ;if zero we aren't showing, skip it
;IsMissileOn   
;  LDA #2         ;otherwise
;  STA ENAM0      ;showit
;  DEC VisibleMissileLine  ;and decrement the missile line thing
;FinishMissile



   DEY      
   BNE ScanLoop   

   LDA #2      
   STA WSYNC   
   STA VBLANK  
   LDX #30     
OverScanWait
   STA WSYNC
   DEX
   BNE OverScanWait
   JMP  MainLoop      


;######## Functions ###########
shootBullet
        LDA #2 ;enable  misile to player 0, it will be enbled by default but will alow to reshoot
        ;sta RESM0
        STA RESMP0
        sta HMCLR

        LDA #0 ;diable  misile to player 0
        STA RESMP0
        LDA #2 ;enable misile graphics
        STA ENAM0 ; and enable misile
        ;STA RESMP0
   LDA #$c9 ; -1 in the left nibble
   STA HMM0 ; of HMM1 sets it to moving
   

        lda #1
   STa MissileEnabled
        
        rts

bulletReset
        INC score
   
      LDA #0 ; disable misile
        STA ENAM0
   LDA #2 ;enable  misile to player 0
        STA RESMP0
        lda #0
   STa MissileEnabled
   rts        

bulletHitTarget
        INC score
   LDA #$c9 ; -1 in the left nibble
   STA HMM0 ; of HMM1 sets it to moving
      LDA #0 ; disable misile
        STA ENAM0
   LDA #2 ;diable  misile to player 0
        STA RESMP0
   rts        

; check controls 
CheckControls
   LDA #%00010000 ;Down?
   BIT SWCHA 
   BNE SkipMoveDown
   INC YPosFromBot
SkipMoveDown

   LDA #%00100000 ;Up?
   BIT SWCHA 
   BNE SkipMoveUp
   DEC YPosFromBot
SkipMoveUp

; for left and right, we're gonna 
; set the horizontal speed, and then do
; a single HMOVE.  We'll use X to hold the
; horizontal speed, then store it in the 
; appropriate register

;assum horiz speed will be zero
   LDX #0   

   LDA #%01000000 ;Left?
   BIT SWCHA 
   BNE SkipMoveLeft
   LDX #$10 ;a 1 in the left nibble means go left
   LDA #%00001000   ;a 1 in D3 of REFP0 says make it mirror
   STA REFP0
SkipMoveLeft
   
   LDA #%10000000 ;Right?
   BIT SWCHA 
   BNE SkipMoveRight
   LDX #$F0 ;a -1 in the left nibble means go right...
   LDA #%00000000
   STA REFP0    ;unmirror it

SkipMoveRight
   STX HMP0 ;set the move for player 0, not the missile like last time...
   ;chek if misile fire
   bit INPT4   ; read button
        bmi NoFireButton ; bit 7 set?
        jsr shootBullet ; 
NoFireButton
   rts


; see if player and missile collide, and change the background color if so
CheckCollision
   LDA #%01000000
   BIT CXM1P      
   BEQ NoTargetHit   ;skip if not hitting...
   LDA YPosFromBot   ;must be a hit! load in the YPos...
        jsr bulletReset
   STA COLUBK  ;and store as the bgcolor
NoTargetHit
       
        LDA #%10000000 ; check bullet in bounds otherwise stop it 
   BIT CXM0FB     
   BEQ NoOutofBounds ;skip if not hitting...
   jsr bulletReset   ;otherwise reset it 
   STA COLUBK  ;and store as the bgcolor
NoOutofBounds
   STA CXCLR   ;reset the collision detection for next time
        rts 

BigHeadGraphic
   .byte #%00111100
   .byte #%01111110
   .byte #%11000001
   .byte #%10111111
   .byte #%11111111
   .byte #%11101011
   .byte #%01111110
   .byte #%00111100
TargetGraphic
   .byte #%00111100
   .byte #%01000010
   .byte #%10111101
   .byte #%10100101
   .byte #%10100101
   .byte #%10111101
   .byte #%01000010
   .byte #%00111100
   org $FFFC
   .word Start
   .word Start