
   processor 6502
        include "vcs.h"
        include "macro.h"
        include "xmacro.h"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; This example demonstrates a scene with a full-screen
; playfield, and a single sprite overlapping it.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        seg.u Variables
   org $80

PFPtr word  ; pointer to playfield data
PFIndex  byte  ; offset into playfield array
PFCount byte   ; lines left in this playfield segment
Temp  byte  ; temporary
YPos0 byte  ; Y position of player sprite
XPos0 byte  ; X position of player sprite
YPos1 byte  ; Y position of player sprite
XPos1 byte  ; X position of player sprite
SpritePtr word  ; pointer to sprite bitmap table
ColorPtr  word  ; pointer to sprite color table

; Temporary slots used during kernel
Bit2p0   byte
Colp0 byte
YP0   byte
YP1   byte
DrawToggle   byte

; Height of sprite in scanlines
SpriteHeight   equ 9

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   seg Code
        org $f000

Start
   CLEAN_START
; Set up initial pointers and player position        
        lda #<PlayfieldData
        sta PFPtr
        lda #>PlayfieldData
        sta PFPtr+1
        lda #<Frame0
        sta SpritePtr
        lda #>Frame0
        sta SpritePtr+1
        lda #<ColorFrame0
        sta ColorPtr
        lda #>ColorFrame0
        sta ColorPtr+1
        lda #195
        sta YPos0
        sta YPos1
        lda #138
        sta XPos1
        lda #15
        sta XPos0
        lda #0
        sta DrawToggle

NextFrame
   VERTICAL_SYNC

; Set up VBLANK timer
   TIMER_SETUP 37
        lda #$18
        sta COLUBK   ; bg color
        lda #$C6
        sta COLUPF   ; fg color
        lda #$68
        sta COLUP0   ; player color
        lda #1
        sta CTRLPF   ; symmetry
        lda #0
        sta PFIndex  ; reset playfield offset
; Set temporary Y counter and set horizontal position
        lda YPos0
        sta YP0      ; yp0 = temporary counter
        lda XPos0
        ldx #0
        jsr SetHorizPos
        sta WSYNC
        sta HMOVE ; gotta apply HMOVE
; Wait for end of VBLANK
   TIMER_WAIT
        lda #0
        sta VBLANK

; Set up timer (in case of bugs where we don't hit exactly)
   TIMER_SETUP 192
        SLEEP 10 ; to make timing analysis work out

NewPFSegment
; Load a new playfield segment.
; Defined by length and then the 3 PF registers.
; Length = 0 means stop
        ldy PFIndex  ; load index into PF array
        lda (PFPtr),y   ; load length of next segment
        bne tempSkip2
        jmp NoMoreSegs  ; == 0, we're done
tempSkip2
        sta PFCount  ; save for later
; Preload the PF0/PF1/PF2 registers for after WSYNC
        iny
        lda (PFPtr),y   ; load PF0
        tax    ; PF0 -> X
        iny
        lda (PFPtr),y   ; load PF1
        sta Temp  ; PF1 -> Temp
        iny
        lda (PFPtr),y   ; load PF2
        iny
        sty PFIndex
        tay    ; PF2 -> Y
; WSYNC, then store playfield registers
; and also the player 0 bitmap for line 2
        sta WSYNC
        stx PF0      ; X -> PF0
        lda Temp
        sta PF1      ; Temp -> PF1
        lda Bit2p0   ; player bitmap
        sta GRP0  ; Bit2p0 -> GRP0
        sty PF2      ; Y -> PF2
; Load playfield length, we'll keep this in X for the loop
        ldx PFCount
KernelDispatcher
    lda DrawToggle
        bit 1
        beq SkipOne
        lda 1
        sta DrawToggle 
        jmp KernelLoop
SkipOne
        lda 0
        sta DrawToggle 
    jmp KernelLoop1
KernelLoop
; Does this scanline intersect our sprite?
        lda #SpriteHeight  ; height in 2xlines
        isb YP0         ; INC yp0, then SBC yp0
        bcs .DoDraw0   ; inside bounds?
        lda #0       ; no, load the padding offset (0)
.DoDraw0
; Load color value for both lines, store in temp var
    pha         ; save original offset on stack
        tay       ;  a -> Y
        lda (ColorPtr),y   ; color for both lines
        sta Colp0    ; -> colp0
; Load bitmap value for each line, store in temp var
    pla         ; pop from stack into a
        asl         ; offset * 2, shift 1 bit left
        tay         ; a -> Y
    lda (SpritePtr),y ; bitmap for first line
        sta Bit2p0      ; -> bit2p0
        iny
    lda (SpritePtr),y ; bitmap for second line
; WSYNC and store values for first line
        sta WSYNC
        sta GRP0  ; Bit1p0 -> GRP0
        lda Colp0
        sta COLUP0   ; Colp0 -> COLUP0
        dex
        beq NewPFSegment   ; end of this playfield segment?
; WSYNC and store values for second line
        sta WSYNC
        lda Bit2p0
        sta GRP0  ; Bit2p0 -> GRP0
        jmp KernelDispatcher

KernelLoop1
; Does this scanline intersect our sprite?
        lda #SpriteHeight  ; height in 2xlines
        isb YP1         ; INC yp0, then SBC yp0
        bcs .DoDraw1    ; inside bounds?
        lda #0       ; no, load the padding offset (0)
.DoDraw1
; Load color value for both lines, store in temp var
    pha         ; save original offset on stack
        tay       ;  a -> Y
        lda (ColorPtr),y   ; color for both lines
        sta Colp0    ; -> colp0
; Load bitmap value for each line, store in temp var
    pla         ; pop from stack into a
        asl         ; offset * 2, shift 1 bit left
        tay         ; a -> Y
    lda (SpritePtr),y ; bitmap for first line
        sta Bit2p0      ; -> bit2p0
        iny
    lda (SpritePtr),y ; bitmap for second line
; WSYNC and store values for first line
        sta WSYNC
        sta GRP1  ; Bit1p0 -> GRP0
        lda Colp0
        sta COLUP1   ; Colp0 -> COLUP0
        dex
        bne tempSkip
        jmp NewPFSegment   ; end of this playfield segment?
tempSkip
; WSYNC and store values for second line
        sta WSYNC
        lda Bit2p0
        sta GRP1  ; Bit2p0 -> GRP0
        jmp KernelDispatcher

NoMoreSegs
; Change colors so we can see when our loop ends
    lda #0
        sta COLUBK
; Wait for timer to finish
        TIMER_WAIT

; Set up overscan timer
    TIMER_SETUP 29
    lda #2
        sta VBLANK
        jsr MoveJoystick0
    jsr MoveJoystick1
        TIMER_WAIT
        jmp NextFrame

SetHorizPos
    sta WSYNC   ; start a new line
    bit 0     ; waste 3 cycles
        sec      ; set carry flag
DivideLoop
         sbc #15     ; subtract 15
         bcs DivideLoop ; branch until negative
         eor #7      ; calculate fine offset
         asl
         asl
         asl
         asl
         sta RESP0,x ; fix coarse position
         sta HMP0,x  ; set fine offset
         rts      ; return to caller

; Read joystick movement and apply to object 0
MoveJoystick0
; Move vertically
; (up and down are actually reversed since ypos starts at bottom)
   ldx YPos0
   lda #%00100000 ;Up?
   bit SWCHA
   bne SkipMoveUp
        cpx #175
        bcc SkipMoveUp
        dex
SkipMoveUp
   lda #%00010000 ;Down?
   bit SWCHA 
   bne SkipMoveDown
        cpx #254
        bcs SkipMoveDown
        inx
SkipMoveDown
   stx YPos0
; Move horizontally
        ldx XPos0
   lda #%01000000 ;Left?
   bit SWCHA
   bne SkipMoveLeft
        cpx #1
        bcc SkipMoveLeft
        dex
SkipMoveLeft
   lda #%10000000 ;Right?
   bit SWCHA 
   bne SkipMoveRight
        cpx #153
        bcs SkipMoveRight
        inx
SkipMoveRight
   stx XPos0
   rts

MoveJoystick1
; Move vertically
; (up and down are actually reversed since ypos starts at bottom)
        ldx YPos1
        lda #%00000010 ;Up?
        bit SWCHA
    bne SkipMoveUp1
    cpx #175
        bcc SkipMoveUp1
        dex
SkipMoveUp1
        lda #%00010000 ;Down?
        bit SWCHA 
        bne SkipMoveDown1
        cpx #254
        bcs SkipMoveDown1
        inx
SkipMoveDown1
    stx YPos1
; Move horizontally
        ldx XPos1
    lda #%01000000 ;Left?
    bit SWCHA
    bne SkipMoveLeft1
        cpx #1
        bcc SkipMoveLeft1
        dex
SkipMoveLeft1
    lda #%10000000 ;Right?
    bit SWCHA 
    bne SkipMoveRight1
        cpx #153
        bcs SkipMoveRight1
        inx
SkipMoveRight1
   stx XPos1
   rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        align $100; make sure data doesn't cross page boundary
PlayfieldData
    .byte  4,#%00000000,#%00000000,#%00000000
    .byte  8,#%00000000,#%00000000,#%00000000
    .byte 15,#%00000000,#%00000000,#%00000000
    .byte 20,#%00000000,#%00000000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%10100000,#%00000000
    .byte  1,#%00000000,#%10100000,#%00000000
    .byte  1,#%00000000,#%10100000,#%00000000
    .byte  1,#%00000000,#%10100000,#%00000000
    .byte  1,#%00000000,#%11100000,#%00000000
    .byte  1,#%00000000,#%11100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000   
    .byte  1,#%00000000,#%00111000,#%00000000
    .byte  1,#%00000000,#%00111000,#%00000000
    .byte  1,#%00000000,#%00101000,#%00000000
    .byte  1,#%00000000,#%00101000,#%00000000
    .byte  1,#%00000000,#%00101000,#%00000000   
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte  1,#%00000000,#%00100000,#%00000000
    .byte 10,#%00000000,#%00000000,#%00000000
    .byte  8,#%00000000,#%00000000,#%00000000
    .byte  4,#%00000000,#%00000000,#%00000000
    .byte 0

; Bitmap data "standing" position
Frame0
   .byte #0
   .byte #0
        .byte #%01101100;$F6
        .byte #%00101000;$86
        .byte #%00101000;$86
        .byte #%00111000;$86
        .byte #%10111010;$C2
        .byte #%10111010;$C2
        .byte #%01111100;$C2
        .byte #%00111000;$C2
        .byte #%00111000;$16
        .byte #%01000100;$16
        .byte #%01111100;$16
        .byte #%01111100;$18
        .byte #%01010100;$18
        .byte #%01111100;$18
        .byte #%11111110;$F2
        .byte #%00111000;$F4

; Color data for each line of sprite
ColorFrame0
   .byte #$FF;
   .byte #$86;
   .byte #$86;
   .byte #$C2;
   .byte #$C2;
   .byte #$36;
   .byte #$36;
   .byte #$25;
   .byte #$F0;

; Epilogue
   org $fffc
        .word Start
        .word Start
