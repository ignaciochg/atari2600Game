; move a happy face with PlayerBufferStuffer
   processor 6502
   include vcs.h
   org $F000

YPosFromBot = $80;
VisiblePlayerLine = $81;
PlayerBuffer = $82 ;setup an extra variable

YPosp1 = $80;
VisiblePlayerLine1 = $81;
PlayerBuffer1 = $82 ;setup an extra variable


;generic start up stuff...
Start
   SEI   
   CLD   
   LDX #$FF 
   TXS   
   LDA #0      
ClearMem 
   STA 0,X     
   DEX      
   BNE ClearMem   
   
   LDA #$00   ;start with a black background
   STA COLUBK  
   LDA #$5C   ;lets go for bright yellow, the traditional color for happyfaces
   STA COLUP0
        LDA #$1C   ;lets go for bright yellow, the traditional color for happyfaces
   STA COLUP1
;Setting some variables...
   LDA #70
   STA YPosFromBot   ;Initial Y Position
   LDA #90
   STA YPosp1  ;Initial Y Position
;; Let's set up the sweeping line. as Missile 1
   LDA #2
   STA ENAM1  ;enable it


   LDA #$20 
   STA NUSIZ1  ;make it quadwidth (not so thin, that)
   STA NUSIZ0  ;make it quadwidth (not so thin, that)
   
   LDA #$F0 ; -1 in the left nibble
   STA HMM1 ; of HMM1 sets it to moving

;VSYNC time
MainLoop
   LDA #2
   STA VSYNC   
   STA WSYNC   
   STA WSYNC   
   STA WSYNC   
   LDA #43  
   STA TIM64T  
   LDA #0
   STA VSYNC   


; for up and down, we INC or DEC
; the Y Position

   LDA #%00010000 ;Down?
   BIT SWCHA 
   BNE SkipMoveDown
   INC YPosFromBot
SkipMoveDown

   LDA #%00100000 ;Up?
   BIT SWCHA 
   BNE SkipMoveUp
   DEC YPosFromBot
SkipMoveUp

; for left and right, we're gonna 
; set the horizontal speed, and then do
; a single HMOVE.  We'll use X to hold the
; horizontal speed, then store it in the 
; appropriate register

;assum horiz speed will be zero
   LDX #0   

   LDA #%01000000 ;Left?
   BIT SWCHA 
   BNE SkipMoveLeft
   LDX #$10 ;a 1 in the left nibble means go left
   LDA #%00001000   ;a 1 in D3 of REFP0 says make it mirror
   STA REFP0
SkipMoveLeft
   
   LDA #%10000000 ;Right?
   BIT SWCHA 
   BNE SkipMoveRight
   LDX #$F0 ;a -1 in the left nibble means go right...
   LDA #%00000000
   STA REFP0    ;unmirror it

SkipMoveRight
   STX HMP0 ;set the move for player 0, not the missile like last time...



; for up and down, we INC or DEC
; the Y Position

   LDA #%00000010 ;Down?
   BIT SWCHA 
   BNE SkipMoveDown1
   INC YPosp1
SkipMoveDown1
   LDA #%00000010 ;Up?
   BIT SWCHA 
   BNE SkipMoveUp1
   DEC YPosp1
SkipMoveUp1

; for left and right, we're gonna 
; set the horizontal speed, and then do
; a single HMOVE.  We'll use X to hold the
; horizontal speed, then store it in the 
; appropriate register

;assum horiz speed will be zero
   LDX #0   
   LDA #%00000100 ;Left?
   BIT SWCHA 
   BNE SkipMoveLeft1
   LDX #$10 ;a 1 in the left nibble means go left
   LDA #%00001000   ;a 1 in D3 of REFP0 says make it mirror
   STA REFP1
SkipMoveLeft1
   
   LDA #%00001000 ;Right?
   BIT SWCHA 
   BNE SkipMoveRight1
   LDX #$F0 ;a -1 in the left nibble means go right...
   LDA #%00000000
   STA REFP1    ;unmirror it

SkipMoveRight1
   STX HMP1 


; see if player and missile collide, and change the background color if so

   LDA #%10000000
   BIT CXM1P      
   BEQ NoCollision   ;skip if not hitting...
   LDA YPosFromBot   ;must be a hit! load in the YPos...
   STA COLUBK  ;and store as the bgcolor
NoCollision
   STA CXCLR   ;reset the collision detection for next time

   LDA #0       ;zero out the buffer
   STA PlayerBuffer ;just in case


WaitForVblankEnd
   LDA INTIM   
   BNE WaitForVblankEnd 
   LDY #191    


   STA WSYNC   
   STA HMOVE   
   
   STA VBLANK     


;main scanline loop...


ScanLoop 
   STA WSYNC   

   LDA #0 
   STA GRP0         ;put it as graphics now
   STA GRP1         ;put it as graphics now



CheckActivatePlayer
   CPY YPosFromBot
   BNE SkipActivatePlayer
   LDA #16
   STA VisiblePlayerLine 
SkipActivatePlayer




;set player bufferto all zeros for this line, and then see if 
;we need to load it with graphic data

;
;if the VisiblePlayerLine is non zero,
;we're drawing it next line
;
   LDX VisiblePlayerLine   ;check the visible player line...
   BEQ FinishPlayer  ;skip the drawing if its zero...
IsPlayerOn  
   LDA BigHeadGraphic-1,X  ;otherwise, load the correct line from BigHeadGraphic
            ;section below... it's off by 1 though, since at zero
            ;we stop drawing
   STA GRP0 ;put that line as player graphic for the next line
   DEC VisiblePlayerLine   ;and decrement the line count
FinishPlayer


CheckActivatePlayer1
   CPY YPosp1
   BNE SkipActivatePlayer1
   LDA #16
   STA VisiblePlayerLine1
SkipActivatePlayer1

   LDX VisiblePlayerLine1  ;check the visible player line...
   BEQ FinishPlayer1 ;skip the drawing if its zero...
IsPlayerOn1 
   LDA BigHeadGraphic2-1,X ;otherwise, load the correct line from BigHeadGraphic
            ;section below... it's off by 1 though, since at zero
            ;we stop drawing
   STA GRP1 ;put that line as player graphic for the next line
   DEC VisiblePlayerLine1  ;and decrement the line count
FinishPlayer1




   DEY      
   BNE ScanLoop   

   LDA #2      
   STA WSYNC   
   STA VBLANK  
   LDX #30     
OverScanWait
   STA WSYNC
   DEX
   BNE OverScanWait
   JMP  MainLoop      

BigHeadGraphic
   .byte #%11111111
   .byte #%11001111
   .byte #%11111111
   .byte #%11111111
   .byte #%11000111
   .byte #%11111111
   .byte #%11000111
   .byte #%11111111
        .byte #%11000111
   .byte #%11111111
   .byte #%11111111
   .byte #%11001111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
BigHeadGraphic2
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11000111
   .byte #%11000111
        .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111
   .byte #%11111111   

   org $FFFC
   .word Start
   .word Start
